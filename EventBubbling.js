console.log('hello world');

const grandparent = document.querySelector(".grandparent");
const parent = document.querySelector(".parent");
const child = document.querySelector(".child");


child.addEventListener("click",()=>{
    console.log("clicked parent");
    
},true);
grandparent.addEventListener("click",()=>{
    console.log("you clicked on grandparent");
},true);

document.addEventListener("click",()=>{
    console.log("clicked on body");
},true);



//event delegation
 grandparent.addEventListener("click",(e)=>{
    console.log(e.target.textContent);
 });


