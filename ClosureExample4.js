
//closure example4
// return func(){
//     return(){
//         if(true){
//             console.log("Hi you called mee")

//         }else{
//             console.log("ive been called already")
//         }
//     }
// }
const myFunc1 = func();
myFunc();
myFunc();
myFunc();

const myFunc2 = func();
myFunc2();
myFunc2();


function func(){
    let counter = 0;
    return function(){
        if(counter < 1){
            console.log("Hi this is call ed");
            counter++;
        }else{
            console.log("Ive been called already");
        }
    }
}

const myFunc = func();
myFunc();
myFunc();